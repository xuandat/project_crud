<?php

use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->delete();
        DB::table('tasks')->insert([
            [
                'id'        =>  1,
                'name'      => 'Practise Laravel',
                'status_id'  => '1',
            ],
            [
                'id'        =>  2,
                'name'      => 'Practise JavaScript,Jquery,Css',
                'status_id'  => '2',
            ],
            [
                'id'        =>  3,
                'name'      => 'Learn VueJS',
                'status_id'  => '3',
            ],
            [
                'id'        =>  4,
                'name'      => 'Gym and Sport',
                'status_id'  => '1',
            ],
            [
                'id'        =>  5,
                'name'      => 'Wash Clothes',
                'status_id'  => '2',
            ]
        ]);
    }
}
