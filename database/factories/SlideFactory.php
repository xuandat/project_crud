<?php

/** @var Factory $factory */

use App\Models\Slide;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Slide::class, function (Faker $faker) {
    return [
        'name'=>$faker->word,
        'image' => $faker->image('public/backend/images/slides',640,480, null, false),
        'content' => $faker->paragraph,
        'link' => $faker->sentence,
    ];
});
