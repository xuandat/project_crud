//add task
$(document).on('click', '.btn-add', function () {
    $('#form-result').html('');
    $('#taskForm')[0].reset();
});
const contentTask = $('#contentTask');
//close modal content task
$(document).on('click', '#contentTask button.close, #contentTask button.btn-close', function () {
    contentTask.removeClass('show');
    contentTask.css('display', 'none');
});

//show task
$(document).on('click', '.btn-show', function () {
    let url = $(this).data('action');
    $.ajax({
        url: url,
    })
        .done((data) => {
            $('#contentTask .modal-content').html(data);
            contentTask.addClass('show');
            contentTask.show();
        })
})

//submit form add
$(document).on('click', '#btn-add-task', function (e) {
    let url = $(this).data('action');
    e.preventDefault();
    submitTaskForm(url, '#taskForm', '#task_table');
});
//add content Task
$(document).on('click', '#add-content', function (e) {
    e.preventDefault();
    let url = $(this).data('action');
    $.ajax({
        url: url,
        method: "post",
        data: {
            'content': CKEDITOR.instances.content.getData(),
        }
    })
        .done((response) => {
            toastr.success(response.message);
            contentTask.removeClass('show');
            contentTask.css('display', 'none');
            $('#task_table tbody').html(response.data);
        })
        .fail((response) => {
            console.log(response);
        })
})

//show edit form
$(document).on('click', '.btn-edit-task', function () {
    let taskId = $(this).attr('data-id');
    let url = $(this).data('action');
    let editTaskModal = $('#editTaskModal');
    $.ajax({
        url: url,
        type: 'get'
    })
        .done((response) => {
            editTaskModal.find('.modal-title').empty();
            editTaskModal.find('.modal-content2').empty();
            editTaskModal.find('.modal-title').append("Sửa công việc # " + taskId);
            editTaskModal.find('.modal-content2').html(response);
        })
});

//update
$(document).on('click', '#btn-update-task', function (e) {
    e.preventDefault();
    let url = $(this).data('action');
    submitTaskForm(url, '#editTaskForm', '#task_table');
});

//delete task
$(document).on('click', '.btn-del-task', function () {
    let url = $(this).data('action');
    deleteRecord(url, '#task');
})

//change status of task
$(document).on('change', '.slt_status', function () {
    let url = $(this).data('action'),
        status_id = $(this).val();
    $.ajax({
        url: url,
        method: "post",
        data: {
            status_id: status_id
        }
    })
        .done((response) => {
            swalAlert('success', response);
            $('#task_table tbody').html(response.data);
        })
        .fail((response) => {
            console.log(response.message);
        })
})

searchRecord('#task_table');

paginate('#task_table');
