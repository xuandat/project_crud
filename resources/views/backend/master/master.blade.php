<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>@yield('title')</title>
    <base href="{{asset('').'backend/'}}">
    @section('css')
        <link rel="stylesheet" href="css/custom.css" />
        <link rel="stylesheet" href="css/b3.css">
        <!-- Bootstrap -->
        <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="build/css/custom.min.css" rel="stylesheet">
        <!--Toast Css-->
        <link rel="stylesheet" href="css/toastr.css">
        <!--SweetAlert-->
        <link href="css/dark.css" rel="stylesheet">
        <script src="css/sweetalert2.min.js"></script>
    @show
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        @include('backend.master.sidebar')
        @include('backend.master.header')
        <!-- page content -->
        <div class="right_col" role="main">
            @yield('content')
        </div>
        <!-- /top tiles -->
    </div>
    <!-- /page content -->
</div>
@section('script')
    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!--Toast JS-->
    <script src="js/backend/toastr.min.js"></script>
    <script src="js/backend/ajax/base.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
    <script src="js/backend/bootstrap.min.js"></script>
    <script src="../ckeditor/ckeditor.js"></script>
    <script src="js/backend/jquery.mark.min.js"></script>
@show
</body>
</html>
