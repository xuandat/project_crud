<div class="modal-header">
    <h5 class="modal-title" id="taskTitle">Công việc <strong>{{$task->name}}</strong></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="" method="post" id="contentTaskForm">
    <div class="modal-body">
        <h5>Nhân viên phụ trách</h5>
        <div class="user-profile">
            @foreach($task->users as $user)
                <img src='images/users/@if($user->image!=""){{$user->image}}@else{{"user.png"}} @endif' alt="" data-toggle="tooltip" title="{{$user->name}}" class="avatar-image">
            @endforeach
        </div>
        <br>
        <label for="content"><h5>Nội dung</h5></label>
        <textarea name="content" id="content" cols="30" rows="10" style="width: 100%;">{!! $task->content !!}</textarea>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-primary" id="add-content"
                data-action="{{ URL('/admin/tasks/content',['task_id'=>$task->id]) }}">Lưu lại
        </button>
    </div>
</form>

<script>
    CKEDITOR.replace('content');
</script>
