<form action="{{route('tasks.update',$task->id)}}" method="post" id="editTaskForm">
    @method('PUT')
    @csrf
    <div id="form_result"></div>
    <div class="modal-body">
        <input type="hidden" name="task_id" value="{{$task->id}}">
        <div class="form-group">
            <label for="">Tiêu đề</label>
            <input type="text" name="name" class="form-control"
                   placeholder="Mời nhập tiêu đề" value="{{$task->name}}">
        </div>
        <div class="form-group">
            <label for="">Người phụ trách</label><br>
            <div class="form-group" style="margin: 0 10px;">
                @foreach ($users as $user)
                    <input type="checkbox" id="chb_{{$user->id}}" name="user[]" value="{{$user->id}}"
                    @foreach($task->users as $item)
                        @if($user->id == $item->id)
                            {{"checked"}}
                            @endif
                        @endforeach
                    >
                    <label for="chb_{{$user->id}}" style="cursor: pointer">{{$user->name}}</label>
                    <br>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <label for="">Trạng thái</label>
            <select class="form-control" name="status" id="">
                @foreach ($status as $item)
                    <option value="{{$item->id}}"
                    @if($item->id == $task->status->id)task_management
                        {{"selected"}}
                        @endif
                    >{{$item->name}}</option>
                @endforeach

            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-info pull-right edit-task" id="btn-update-task"
                data-action="{{route('tasks.update',$task->id)}}">Lưu lại
        </button>
        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
    </div>
</form>
