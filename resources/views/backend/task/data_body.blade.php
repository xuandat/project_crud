@foreach ($tasks as $task)    <div id="form-result"></div>
    <tr id="task{{$task->id}}">
        <td>{{$task->id}}</td>
        <td class="task-name">{{$task->name}}</td>
        <td class="task-user">
            @foreach ($task->users as $user)
                <button type="button" class="btn btn-info btn-show-staff" data-id="{{$user->id}}"
                        data-target="">{{$user->name}}</button>
            @endforeach
        </td>
        <td>
            {{Carbon\Carbon::parse($task->created_at)->format('H:m:s d-m-Y')}}
        </td>
        <td>
            <select class="browser-default custom-select slt_status
                    @if($task->status->id==1){{"to-do"}}
            @elseif($task->status->id==2){{"doing"}}
            @elseif($task->status->id==3){{"checking"}}
            @elseif($task->status->id==4){{"confirm"}}
            @elseif($task->status->id==5){{"done"}}
            @endif"
                    data-id="{{$task->id}}" data-action="{{URL('/admin/tasks/change_status',['task_id'=>$task->id])}}">
                @foreach($status as $item)
                    <option value="{{$item->id}}"
                    @if($item->id==$task->status->id)
                        {{"selected"}}
                        @endif
                    >
                        {{$item->name}}
                    </option>

                @endforeach

            </select>
        </td>
        <td>
            <button class="btn btn-primary btn-show" data-action="{{ route('tasks.show',$task->id) }}">
                <i class="fa fa-eye" title="Xem"></i>
            </button>
            <button class="btn btn-warning btn-edit-task" data-action="{{route('tasks.edit',$task->id)}}"
                    data-toggle="modal" data-target="#editTaskModal" data-id="{{$task->id}}">
                <i class="fa fa-edit" title="Sửa"></i>
            </button>
            <button class="btn btn-danger btn-del-task" data-action="{{route('tasks.destroy',$task->id)}}">
                <i class="fa fa-remove" title="Xóa"></i>
            </button>
        </td>
    </tr>
@endforeach
<tr>
    <td colspan="6" align="center">
        {{$tasks->links()}}
    </td>
</tr>

