@extends('backend.master.master')
@section('css')
    @parent
    <link rel="stylesheet" href="css/task.css">
@endsection
@section('title','Task Management')
@section('content')
    <h1 class="text-center">Danh sách công việc</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-push-9">
                <button type="button" class="btn btn-info btn-add" data-toggle="modal" data-target="#addTaskModal">Thêm
                    công việc
                </button>
            </div>
            <div class="col-md-9">
                <form data-action="{{ route('tasks.fetch_data') }}" method="get" id="fetchDataForm"
                      style="width: 100%">
                    <input type="text" name="txt_search" id="txt_search"
                           style="float: right;border-radius: 5px;padding: 5px;"
                           placeholder="Tìm kiếm....">
                </form>
            </div>
        </div>

        <div class="panel panel-success table-responsive">
            <table class="table table-hover table-striped" id="task_table">
                <thead>
                <tr class="table-info">
                    <th>#</th>
                    <th>Công việc</th>
                    <th>Nhân viên phụ trách</th>
                    <th>Ngày bắt đầu</th>
                    <th>Trạng thái</th>
                    <th>Tùy chọn</th>
                </tr>
                </thead>
                <tbody>
                @include('backend.task.data_body')
                </tbody>

            </table>
            <input type="hidden" name="hidden_page" id="hidden_page" value="1"/>
        </div>
        <!-- Add Task Modal -->
        <div id="addTaskModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div id="form-result"></div>
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm công việc</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="{{route('tasks.store')}}" method="post" id="taskForm">
                        @csrf
                        <div id="form_result"></div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">Tiêu đề</label>
                                <input type="text" name="name" class="form-control"
                                       placeholder="Mời nhập tiêu đề">

                            </div>
                            <div class="form-group">
                                <label for="">Người phụ trách</label>
                                <br>
                                <div class="form-group" style="margin: 0 10px;">
                                    @foreach ($users as $user)
                                        <input type="checkbox" id="chb_{{$user->id}}" name="user[]"
                                               value="{{$user->id}}">
                                        <label for="chb_{{$user->id}}" style="cursor: pointer">{{$user->name}}</label>
                                        <br>
                                    @endforeach
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <select class="form-control" name="status_id" id="">
                                    @foreach ($status as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info pull-right" id="btn-add-task"
                                    data-action="{!! route('tasks.store') !!}">Lưu lại
                            </button>
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <!--Add Content Task-->
        <div class="modal fade" id="contentTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 130%;">

                </div>
            </div>
        </div>
        <!--End-->


        <!--Edit Task Modal-->
        <div id="editTaskModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Sửa công việc</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-content2">
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    @parent
    <script src="js/backend/ajax/task.js"></script>
@endsection
