@extends('backend.master.master')
@section('title','Category Management')
@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Thể loại
                    </h1>
                </div>
                <div class="col-lg-12">
                    @if (session('thongbao'))
                        <div class="alert alert-success" role="alert">
                            <strong>{{session('thongbao')}}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-md-3 col-md-push-9">
                    <button type="button" class="btn btn-info btn-show-add" data-toggle="modal"
                            data-target="#addCategoryModal">Thêm thể loại
                    </button>
                </div>
                <!--Add category-->
                <div id="addCategoryModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Thêm danh mục</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form method="post" id="addCategoryForm">
                                @csrf
                                <div id="form_result"></div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Tiêu đề</label>
                                        <input type="text" name="name" class="form-control"
                                               placeholder="Mời nhập tiêu đề">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-info pull-right" id="btn-add-category"
                                            data-action="{{route('categories.store')}}">Lưu lại
                                    </button>
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <!--end-add-category-->

                <!--Edit category-->
                <div id="editCategoryModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">

                        </div>
                    </div>
                </div>
                <!--end-add-category-->
                <!-- /.col-lg-12 -->
                <table class="table table-striped table-bordered table-hover" id="category_table">
                    <thead>
                    <tr align="center">
                        <th>#</th>
                        <th>Tên</th>
                        <th>Tên không dấu</th>
                        <th>Sửa</th>
                        <th>Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @include('backend.category.data_body')
                    </tbody>
                </table>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@endsection

@section('script')
    @parent
    <script src="js/backend/ajax/category.js"></script>
@endsection
