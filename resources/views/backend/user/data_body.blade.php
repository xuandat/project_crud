@foreach($users as $user)
    <tr id="user{{$user->id}}">
        <td>#{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>
            @if($user->image!= "")
                <div class='user-profile'><img class="avatar-image" src='images/users/{{$user->image}}' alt=''
                                               title="{{$user->name}}"></div>
            @else
                <div class='user-profile'><img class="avatar-image" src='images/users/user.png' alt=''
                                               title="{{$user->name}}"></div>
            @endif
        </td>
        <td>{{$user->email}}</td>
        <td>{{$user->address}}</td>
        <td>{{$user->phone}}</td>
        <td>
            @foreach($user->roles as $role)
                <label class="badge badge-success">{{ $role->name }}</label>
            @endforeach
        </td>
        <td>
            <button type="button" data-info='@json($user)' name="edit" id="{{$user->id}}"
                    class="edit btn btn-primary btn-sm" data-toggle="modal"
                    data-target="#editUserModal" data-action="{{route('users.edit',$user->id)}}" title="Sửa">Sửa
            </button>
            <button type="button" name="delete" id="{{$user->id}}" class="delete btn btn-danger btn-sm"
                    data-action="{{ route('users.destroy',$user->id) }}" title="Xóa">Xóa
            </button>
        </td>
    </tr>
@endforeach
<tr>
    <td colspan="8">
        {{$users->links()}}
    </td>
</tr>

