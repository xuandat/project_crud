<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//BACKEND
Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix' => 'admin', 'namespace' => 'Backend\Admin', 'middleware' => 'auth'], function () {
    Route::get('', 'IndexController@index');
    Route::resource('users', 'UserController')->middleware('role:superAdmin|admin');
    Route::get('/users/pagination/fetch_data', 'UserController@fetchData')->name('users.fetch_data');
    Route::group(['prefix' => 'tasks'], function () {
        Route::post('change_status/{id}', 'TaskController@changeStatus')->name('task.status');
        Route::post('content/{id}', 'TaskController@updateContent')->name('task.content');
        Route::get('pagination/fetch_data', 'TaskController@fetchData')->name('tasks.fetch_data');
        Route::get('/charts', 'TaskController@doChart')->middleware('permission:edit-user|delete-user');
    });
    Route::resource('tasks', 'TaskController');
    Route::resource('statuses', 'StatusController');
    Route::resource('categories', 'CategoryController');
    Route::get('/categories/pagination/fetch_data', 'CategoryController@fetchData')->name('category.fetchData');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
