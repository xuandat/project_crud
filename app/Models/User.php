<?php

namespace App\Models;

use App\Traits\HandleUploadImage;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;
    use HandleUploadImage;

    protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        return $this->attributes['name'] . '-' . $this->attributes['id'];
    }

    protected $uploadPath = 'backend/images/users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'image',
        'address',
        'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'user_task');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function isAdmin()
    {
        if (Auth::check()) {
            foreach (Auth::user()->roles() as $role) {
                if (strpos($role, 'admin') !== false) {
                    return true;
                }
            }
        }
    }

    public function getPaginate($paginateNumber)
    {
        return $this->with('roles')->latest('id')->paginate($paginateNumber);
    }

    public function getAll()
    {
        return $this->with('tasks')->latest('id')->get();
    }

    public function searchUser($searchData, $paginateNumber)
    {
        $searchData = str_replace(" ", "%", $searchData);
        return $this->where('name', 'LIKE', '%' . $searchData . '%')
            ->orWhere('email', 'LIKE', '%' . $searchData . '%')
            ->orWhere('address', 'LIKE', '%' . $searchData . '%')
            ->with('roles')
            ->latest('id')
            ->paginate($paginateNumber);
    }

    public function store($requestData)
    {
        $user = $this->create($requestData);
        $user->roles()->attach($requestData['role']);
        return $user;
    }

    public function updateUser(array $requestData, $id)
    {
        $user = $this->findOrFail($id);
        $user->update($requestData);
        $user->roles()->sync($requestData['role']);
        return $user;
    }

    public function deleteUser($id)
    {
        $user = $this->findOrFail($id);
        $user->delete();
        return $user;
    }

    public function register($requestData)
    {
        $requestData['image'] = 'user.png';
        $user = $this->create($requestData);
        $user->roles()->attach(Role::where('name', 'employee')->first());
        return $user;
    }

    public function getTaskByUser($userId, $paginateNumber)
    {
        if (Auth::user()->isAdmin()) {
            $tasks = Task::latest()->with('users')->with('status')->paginate($paginateNumber);
        } else {
            $tasks = $this->where('id',
                $userId)->firstorFail()->tasks()->with('users')->with('status')->paginate($paginateNumber);
        }
        return $tasks;
    }

    public function scopeGetAddress($query, $address)
    {
        return $query->where('address', 'like', '%' . $address . '%');
    }

    public function scopeRole($query, $role)
    {
        return $query->where('role', $role);
    }

    public function getRoleByUser($id)
    {
        $user = $this->findOrFail($id);
        return $user->roles()->pluck('role_id')->toArray();
    }

    public function hasRole($role)
    {
        $roleItem = explode('|', $role);
        $checkRole = Role::whereIn('name', $roleItem)->pluck('id')->toArray();
        if ($this::where('id', '=', $this->id)->whereHas('roles', function ($query) use ($checkRole) {
            $query->whereIn('role_id', $checkRole);
        })->exists()) {
            return true;
        } else {
            return false;
        }
    }

    public function hasPermission($permission)
    {
        $permissionItem = explode('|', $permission);
        $roles = Role::whereHas('permissions', function ($query) use ($permissionItem) {
            $query->whereIn('name', $permissionItem);
        })->pluck('id')->toArray();
        if ($this::where('id', '=', $this->id)->whereHas('roles', function ($query) use ($roles) {
            $query->whereIn('role_id', $roles);
        })->exists()) {
            return true;
        } else {
            return false;
        }
    }


}
