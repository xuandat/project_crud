<?php
function slim_echo($e)
{
    echo "<pre>";
    var_dump($e);
    echo "</pre>";
}

function showError($errors, $inputName)
{
    if ($errors->has($inputName)) {
        echo '<div class="alert alert-danger"><strong>';
        echo $errors->first($inputName);
        echo '</strong></div>';
    }
}
